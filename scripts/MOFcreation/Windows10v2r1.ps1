configuration Windows10v2r1
{
    param()
    Import-DscResource -ModuleName PowerSTIG -ModuleVersion 4.7.1
    Import-DscResource -ModuleName PSDesiredStateConfiguration
    Node localhost
    {
        WindowsClient BaseLine
        {
            OsVersion   = '10'
            StigVersion = '2.1'
            SkipRule    = 'V-220972','V-220957','V-220725','V-14248','V-220850','V-220851','V-220852','V-220866','V-220810','V-220957','V-220868'
            Exception   = @{
                'V-220799' = @{
                    ValueData = '1' # Required for using Azure Image Builder access to creation
                }
                'V-220968' = @{
                    Identity = 'Guests' 
                }
            }
        }
        Chrome ChromeSettings
        {
            StigVersion = '2.1'
        }
        Office OfficeSystem
        {
            OfficeApp = 'System2016'
            Stigversion    = '1.1'
        }
        Registry EnableFSLogix
        {
            Ensure = "Present"
            Key = "HKLM:\SOFTWARE\FSLogix\Profiles"
            ValueName = "Enabled"
            ValueData = "1"
            ValueType = "Dword"
        }
        Registry FSLogixSharePath
        {
            Ensure = "Present"
            Key = "HKLM:\SOFTWARE\FSLogix\Profiles"
            ValueName = "VHDLocations"
            ValueData = "//safslogx.file.core.windows.net/fslogixprofiles"
            ValueType = "REG_SZ"
        }
        Registry FSLogixWipe
        {
            Ensure = "Present"
            Key = "HKLM:\SOFTWARE\FSLogix\Profiles"
            ValueName = "DeleteLocalProfileWhenVHDShouldApply"
            ValueData = "1"
            ValueType = "Dword"
        }
        Registry FSLogixWipe
        {
            Ensure = "Present"
            Key = "HKLM:\SOFTWARE\FSLogix\Profiles"
            ValueName = "FlipFlopProfileDirectoryName"
            ValueData = "1"
            ValueType = "Dword"
        }
    }
}
Windows10v2r1  -Output .\imagebuilder