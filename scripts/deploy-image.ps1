$temp_path = "https://bitbucket.org/rmcsheffery/project-stig/raw/186de3b6e34683b49e9d0d9ba3e80f36b2e505cc/azuredeploy.json"
$imageResourceGroup = "rg-gates-WVD-Demo1-eastus"
$deploymentName = "rg-gates-WVD-Demo1-eastus"

##### Only Needed on first ever deployment in subscription
# Register-AzProviderFeature -FeatureName VirtualMachineTemplatePreview -ProviderNamespace Microsoft.VirtualMachineImages
# Register-AzResourceProvider -ProviderNamespace Microsoft.KeyVault
# Register-AzResourceProvider -ProviderNamespace Microsoft.VirtualMachineImages
# Register-AzResourceProvider -ProviderNamespace Microsoft.Compute
# Register-AzResourceProvider -ProviderNamespace Microsoft.Storage

# New-AzDeployment `
#   -Name $deploymentName `
#   -TemplateUri $temp_path `
#   -rgName $imageResourceGroup `
#   -rgLocation 'East US' `
#   -Location 'East US' `
#   -DeploymentDebugLogLevel All
 #  `




# Invoke-AzResourceAction `
#   -ResourceName 'Win10WVDw365_STIG' `
#   -ResourceGroupName 'rg-gates-WVD-Demo1-eastus' `
#   -ResourceType Microsoft.VirtualMachineImages/imageTemplates `
#   -ApiVersion "2020-02-14" `
#   -Action Run `
#   -Force

  $imageTemplates = Get-AzImageBuilderTemplate -ResourceGroupName 'rg-gates-WVD-Demo-eastus'
foreach($template in $imageTemplates){
    Start-AzImageBuilderTemplate -ResourceGroupName 'rg-gates-WVD-Demo-eastus' -Name $template.Name -AsJob
}
# Get-AzResourceGroup | where {$_.ResourceGroupName -like 'IT_rg-gates-WVDstig-rm0-eastus_Win20*'}|select ResourceGroupName| Remove-AzResourceGroup -Confirm